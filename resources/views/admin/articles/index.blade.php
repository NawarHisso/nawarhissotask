@extends('admin.layouts.app')

@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}} ">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}} ">
@endsection

@section('navHead')
<h3>Articles
    @can('articles.create', Auth::user())
    <a href="{{ route('article.create') }}"><small>| Create new.</small></a>
    @endcan
</h3>
@endsection

@section('content')
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
        <table id="articleTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>Is published</th>
                    <th>Posted by</th>
                    <th>Updated by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($articles as $article)
                <tr>
                    <td>{{ $article->id }}</td>
                    <td>{{ $article->title }}</td>
                    <td>{{ $article->subtitle }}</td>
                    <td>
                        @if ($article->status == 1)
                        Published
                        @else
                        Not published
                        @endif
                    </td>
                    <td>{{ $article->userCreated->name }}</td>
                    <td>{{ $article->userUpdated->name }}</td>
                    <td><a href="{{ route('article.edit', $article->id) }}"><span
                                class="oi oi-external-link"></span></a></td>
                    <td>
                        @can('articles.delete', Auth::user())
                        @if (Auth::user()->id == $article->userCreated->id)
                        <form id="deleteForm{{$article->id}}" method="POST"
                            action="{{ route('article.destroy', $article->id) }}" style="display: none">
                            @csrf
                            @method('DELETE')
                        </form>
                        <a href="#"
                            onclick="event.preventDefault();if(confirm('Are you sure to delete Article#{{$article->id}}?')){document.getElementById('deleteForm{{$article->id}}').submit();}"><span
                                class="oi oi-trash"></span></a>
                        @endif
                        @endcan
                    </td>
                </tr>
                @empty
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Subtitle</th>
                    <th>Is published</th>
                    <th>Posted by</th>
                    <th>Updated by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
      $("#articleTable").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
    });
</script>
@endsection