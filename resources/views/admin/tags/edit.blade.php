@extends('admin.layouts.app')

@section('navHead')
<a href="{{ route('tag.index') }}">
    <h3>Tags
    </h3>
</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form role="form" action="{{ route('tag.update', $tag->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-12">

                    <div class="card-header">
                        <h3 class="card-title">
                            Update tag#{{ $tag->id }}
                            <small>| What is in your mind?</small>
                        </h3>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>Tag name</label>
                                    <input type="text" class="form-control" value="{{ $tag->name }}" name="name"
                                        id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form-group">
                        <input type="submit" class="btn btn-warning" value="Update">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection