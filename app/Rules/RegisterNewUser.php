<?php

namespace App\Rules;

use App\AdminUser;
use Illuminate\Contracts\Validation\Rule;

class RegisterNewUser implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = AdminUser::where('email', $value)->first();
        if ($user) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'You are not one of HaykalMedia staff.';
    }
}
