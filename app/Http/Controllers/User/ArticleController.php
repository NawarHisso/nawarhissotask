<?php

namespace App\Http\Controllers\User;

use App\Article;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function article(Article $article)
    {
        return view('user.article', compact('article'));
    }
}
