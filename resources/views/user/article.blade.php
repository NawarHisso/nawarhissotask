@extends('user.layouts.app')

@section('bg-img', asset('storage/images/'.$article->image))

@section('title', $article->title)

@section('subheading', $article->subtitle)

@section('content')
<!-- Article Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <small>Created at {{ $article->created_at->diffForHumans() }}</small>
                @foreach ($article->categories as $category)
                <small class="float-right" style="margin-right: 20px;">
                    <a href="{{ route('category', $category->id) }}">{{ $category->name }}</a>
                </small>
                @endforeach

                <br><br>

                {!! htmlSpecialChars_decode($article->body) !!}

                <br><br>

                <h3>Tags</h3>
                @foreach ($article->tags as $tag)
                <a href="{{ route('tag', $tag->id) }}">
                    <small class="float-left"
                        style="margin-right: 20px; border-radius: 5px; border: 1px solid gray; padding: 5px">
                        {{ $tag->name }}
                    </small>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</article>

<hr>
@endsection