<?php

namespace App\Http\Controllers\User;

use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Tag;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $articles = Article::where('status', 1)->orderBy('created_at', 'desc')->paginate(3);
        return view('user.welcome', compact('articles'));
    }

    public function tag(Tag $tag)
    {
        $articles = $tag->articles();
        return view('user.welcome', compact('articles'));
    }

    public function category(Category $category)
    {
        $articles = $category->articles();
        return view('user.welcome', compact('articles'));
    }
}
