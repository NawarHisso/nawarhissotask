@extends('admin.layouts.app')

@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}} ">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}} ">
@endsection

@section('navHead')
<h3>Users
    <a href="{{ route('adminUser.create') }}"><small>| Create new.</small></a>
</h3>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <table id="adminUserTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>U.Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($adminUsers as $adminUser)
                <tr>
                    <td>{{ $adminUser->id }}</td>
                    <td>{{ $adminUser->name }}</td>
                    <td>{{ $adminUser->email }}</td>
                    <td>
                        <ul>
                        @foreach ($adminUser->roles as $role)
                        <li>
                            {{ $role->name }}
                        </li>
                        @endforeach
                        </ul>
                    </td>
                    <td>
                        @if (Auth::user()->email <> $adminUser->email)
                        <a href="{{ route('adminUser.edit', $adminUser->id) }}"><span
                            class="oi oi-external-link"></span></a></td>
                        @endif
                    <td>
                        @if (Auth::user()->email <> $adminUser->email)
                        <form id="deleteForm{{$adminUser->id}}" method="POST"
                            action="{{ route('adminUser.destroy', $adminUser->id) }}" style="display: none">
                            @csrf
                            @method('DELETE')
                        </form>
                        <a href="#"
                            onclick="event.preventDefault();if(confirm('Are you sure to delete User#{{$adminUser->id}}?')){document.getElementById('deleteForm{{$adminUser->id}}').submit();}"><span
                                class="oi oi-trash"></span></a>
                        @endif
                    </td>
                </tr>
                @empty
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th>U.Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
      $("#adminUserTable").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
    });
</script>
@endsection