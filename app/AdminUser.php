<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    protected $fillable = [
        'name', 'email',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'admin_user_roles')->withTimestamps();
    }
}
