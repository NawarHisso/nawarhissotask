@extends('admin.layouts.app')

@section('navHead')
<a href="{{ route('adminUser.index') }}">
    <h3>Users
    </h3>
</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form role="form" action="{{ route('adminUser.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card-header">
                        <h3 class="card-title">
                            Create a new user
                            <small>| Go ahead!</small>
                        </h3>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>User name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>User email</label>
                                    <input type="email" class="form-control" name="email" id="email"
                                        placeholder="something@HaykalMedia.com">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>Roles</label>
                                    <select class="select2" name="roles[]" id="roles" multiple="multiple"
                                        data-placeholder="Select a role" style="width: 100%;">
                                        <option value="">-</option>
                                        @foreach ($roles as $role)
                                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form-group">
                        <input type="submit" class="btn btn-primary" value="Create">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $(function () {
      $('.select2').select2()
    })
</script>
@endsection