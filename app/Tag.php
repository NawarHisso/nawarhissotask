<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    public function userCreated()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function userUpdated()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function articles()
    {
        return $this->belongsToMany('App\Article', 'article_tags')->orderBy('created_at', 'desc')->paginate(3);
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}
