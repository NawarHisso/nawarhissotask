@extends('admin.layouts.app')

@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}} ">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}} ">
@endsection

@section('navHead')
<h3>Categories
    <a href="{{ route('category.create') }}"><small>| Create new.</small></a>
</h3>
@endsection

@section('content')
<div class="card">
    <!-- /.card-header -->
    <div class="card-body">
        <table id="categoryTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>Name</th>
                    <th>Created by</th>
                    <th>Updated by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->userCreated->name }}</td>
                    <td>{{ $category->userUpdated->name }}</td>
                    <td><a href="{{ route('category.edit', $category->id) }}"><span
                                class="oi oi-external-link"></span></a></td>
                    <td>
                        @if (Auth::user()->id == $category->userCreated->id)
                        <form id="deleteForm{{$category->id}}" method="POST"
                            action="{{ route('category.destroy', $category->id) }}" style="display: none">
                            @csrf
                            @method('DELETE')
                        </form>
                        <a href="#"
                            onclick="event.preventDefault();if(confirm('Are you sure to delete Category#{{$category->id}}?')){document.getElementById('deleteForm{{$category->id}}').submit();}"><span
                                class="oi oi-trash"></span></a>
                        @endif
                    </td>
                </tr>
                @empty
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th>S.No</th>
                    <th>Name</th>
                    <th>Created by</th>
                    <th>Updated by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
      $("#categoryTable").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
    });
</script>
@endsection