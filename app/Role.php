<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany('App\AdminUser', 'admin_user_roles')->withTimestamps();
    }
}
