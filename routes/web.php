<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'User'], function () {
    Route::get('/', 'HomeController@index')->name('welcome');

    Route::get('/article/{article?}', 'ArticleController@article')->name('article');

    Route::get('/article/tag/{tag?}', 'HomeController@tag')->name('tag');

    Route::get('/article/category/{category?}', 'HomeController@category')->name('category');
});

Route::group(['namespace' => 'Admin'], function () {
    Route::get('administration/home', 'HomeController@index')->name('admin.home');

    Route::resource('administration/user', 'UserController');

    Route::resource('administration/adminUser', 'AdminUserController');

    Route::resource('administration/role', 'RoleController');

    Route::resource('administration/article', 'ArticleController');

    Route::resource('administration/tag', 'TagController');

    Route::resource('administration/category', 'CategoryController');
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
