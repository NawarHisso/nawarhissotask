@extends('user.layouts.app')

@section('bg-img', asset('user/img/contact-bg.jpg'))

@section('title', 'Login')

@section('subheading', '')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            <p>Welcome again!</p>
            <form action="{{ route('login') }}" method="POST" name="login" id="login">
                @csrf
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Email Address</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                            placeholder="somthing@HakalMedia.com" name="email" id="email" value="{{ old('email') }}" required
                            autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="control-group">
                    <div class="form-group floating-label-form-group controls">
                        <label>Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                            placeholder="Password" id="password" name="password" required
                            autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <br>
                <input type="submit" class="btn btn-primary" id="submit" value="Login">
                <a href="{{ route('register') }}"  class="btn btn-primary">Register</a>
            </form>
        </div>
    </div>
</div>
@endsection