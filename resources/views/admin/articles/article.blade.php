@extends('admin.layouts.app')

@section('navHead')
<a href="{{ route('article.index') }}">
    <h3>Articles
    </h3>
</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form role="form" method="POST" action="{{ route('article.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <div class="card-header">
                        <h3 class="card-title">
                            Write an article
                            <small>| What is in your mind?</small>
                        </h3>
                    </div>
                    <div class="card-body pad">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Article title</label>
                                    <input type="text" class="form-control" name="title" id="title" placeholder="Title">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Article subtitle</label>
                                    <input type="text" class="form-control" name="subtitle" id="subtitle"
                                        placeholder="Subtitle">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputFile">Article image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="image" id="image">
                                            <label class="custom-file-label" for="image">Choose image</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputFile">Publish Article</label>
                                    <div class="form-check">
                                        <input type="checkbox" value="1" class="form-check-input" name="status"
                                            id="status">
                                        <label class="form-check-label" for="status">Publish</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Categories</label>
                                    <select class="select2" name="categories[]" id="categories" multiple="multiple"
                                        data-placeholder="Select a category" style="width: 100%;">
                                        <option value="">-</option>
                                        @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tags</label>
                                    <select class="select2" name="tags[]" id="tags" multiple="multiple"
                                        data-placeholder="Select a tag" style="width: 100%;">
                                        <option value="">-</option>
                                        @foreach ($tags as $tag)
                                        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <textarea class="textarea" placeholder="Place some text here" name="body" id="body"
                                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-primary" value="Post">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $(function () {
      $('.select2').select2()
    })
</script>
@endsection