<!DOCTYPE html>
<html>

<head>
    @include('admin.layouts.head')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    @include('admin.layouts.navbar')
    @include('admin.layouts.sidebar')
    <div class="wrapper">
        <div class="content-wrapper">
            @yield('content')
        </div>
    </div>
    @include('admin.layouts.footer')
</body>

</html>