@extends('admin.layouts.app')

@section('navHead')
<a href="{{ route('role.index') }}">
    <h3>Roles
    </h3>
</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form role="form" action="{{ route('role.store') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-12">

                    <div class="card-header">
                        <h3 class="card-title">
                            Create a new role
                            <small>| Go ahead!</small>
                        </h3>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>Role name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form-group">
                        <input type="submit" class="btn btn-primary" value="Create">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection