<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('welcome') }}" class="brand-link">
        <img src="{{ asset('admin/dist/img/HaykalMediaLogo.jpg') }}" alt="HaykalMedia Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">HaykalMedia</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('admin/dist/img/NawarHisso.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{ route('admin.home') }}" class="d-block">{{ Auth::user()->name }} (Home)</a>
            </div>
        </div>
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('article.index') }}" @if(Str::startsWith(Route::current()->getName(),
                                'article'))
                                class="nav-link active" @else class="nav-link" @endif>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Articles</p>
                            </a>
                        </li>
                        @can('articles.create', Auth::user())
                        <li class="nav-item">
                            <a href="{{ route('category.index') }}" @if(Str::startsWith(Route::current()->getName(),
                                'category'))
                                class="nav-link active" @else class="nav-link" @endif>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Categories</p>
                            </a>
                        </li>
                        @endcan
                        @can('articles.create', Auth::user())
                        <li class="nav-item">
                            <a href="{{ route('tag.index') }}" @if(Str::startsWith(Route::current()->getName(), 'tag'))
                                class="nav-link active" @else class="nav-link" @endif>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tags</p>
                            </a>
                        </li>
                        @endcan
                        @can('articles.create', Auth::user())
                        <li class="nav-item">
                            <a href="{{ route('adminUser.index') }}" @if(Str::startsWith(Route::current()->getName(),
                                'adminUser'))
                                class="nav-link active" @else class="nav-link" @endif>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        @endcan
                        @can('articles.create', Auth::user())
                        <li class="nav-item">
                            <a href="{{ route('role.index') }}" @if(Str::startsWith(Route::current()->getName(),
                                'role'))
                                class="nav-link active" @else class="nav-link" @endif>
                                <i class="far fa-circle nav-icon"></i>
                                <p>Roles</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Logout</p>
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>