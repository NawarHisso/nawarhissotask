@extends('admin.layouts.app')

@section('navHead')
<a href="{{ route('role.index') }}">
    <h3>Roles
    </h3>
</a>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form role="form" action="{{ route('role.update', $role->id) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-md-12">
                    <div class="card-header">
                        <h3 class="card-title">
                            Update role#{{ $role->id }}
                            <small>| Go ahead!</small>
                        </h3>
                    </div>
                    <div class="card-body pad">
                        <div class="box-body">
                            <div class="col-lg-offset-4 col-lg-6">
                                <div class="form-group">
                                    <label>Role name</label>
                                    <input type="text" class="form-control" value="{{ $role->name }}" name="name"
                                        id="name" placeholder="Name">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer form-group">
                        <input type="submit" class="btn btn-warning" value="Update">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('footer')
<script src="{{ asset('admin/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('admin/plugins/select2/js/select2.min.js') }}"></script>
<script>
    $(function () {
      $('.select2').select2()
    })
</script>
@endsection