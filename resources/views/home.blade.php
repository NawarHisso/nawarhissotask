@extends('user.layouts.app')

@section('bg-img', asset('user/img/home-bg.jpg'))

@section('title', 'Welcome Home!')

@section('subheading', '')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    Go to <a href="{{ route('admin.home') }}">Dashboard</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection