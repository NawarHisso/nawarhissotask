<?php

namespace App\Http\Controllers\admin;

use App\AdminUser;
use App\Article;
use App\Category;
use App\Http\Controllers\Controller;
use App\Tag;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::forUser(Auth::user())->allows('articles.create')) {
            $tags = Tag::all();
            $categories = Category::all();
            return view('admin.articles.article', compact(['tags', 'categories']));
        }

        return redirect(route('admin.home'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'body' => 'required',
            'image' => 'required'
        ]);

        //Article::create($request->all());

        $article = new Article();
        $article->title = $request->title;
        $article->subtitle = $request->subtitle;
        $article->body = $request->body;
        if ($request->hasFile('image')) {
            $fileName  = $request->image->getClientOriginalName();
            $request->image->storeAs('images', $fileName, 'public');
            $article->image = $fileName;
        }
        $article->status = $request->status ? $request->status : false;
        $article->created_by = Auth::user()->id;
        $article->updated_by = Auth::user()->id;
        $article->save();

        $article->tags()->sync($request->tags);
        $article->categories()->sync($request->categories);

        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::with('tags', 'categories')->where('id', $id)->first(); // return $article;
        $tags = Tag::all();
        $categories = Category::all();
        return view('admin.articles.edit', compact(['article', 'tags', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'subtitle' => 'required',
            'body' => 'required',
            'image' => 'required'
        ]);

        $article = Article::find($id);

        if ($request->hasFile('image')) {
            $fileName  = $request->image->getClientOriginalName();
            if ($article->image) {
                Storage::delete('/storage/app/public/images' . $article->image);
            }
            $request->image->storeAs('images', $fileName, 'public');
            $article->image = $fileName;
        }

        $article->title = $request->title;
        $article->subtitle = $request->subtitle;
        $article->body = $request->body;
        $article->status = $request->status ? $request->status : false;
        $article->updated_by = Auth::user()->id;
        $article->tags()->sync($request->tags);
        $article->categories()->sync($request->categories);

        $article->save();

        return redirect(route('article.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Article::where('id', $id)->delete();

        $articles = Article::all();
        return view('admin.articles.index', compact('articles'));
    }
}
