@extends('admin.layouts.app')

@section('head')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}} ">
<link rel="stylesheet" href="{{ asset('admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}} ">
@endsection

@section('navHead')
<h3>Roles
    <a href="{{ route('role.create') }}"><small>| Create new.</small></a>
</h3>
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <table id="roleTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>R.Id</th>
                    <th>Name</th>
                    <th>Created by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($roles as $role)
                <tr>
                    <td>{{ $role->id }}</td>
                    <td>{{ $role->name }}</td>
                    <td>User</td>
                    <td><a href="{{ route('role.edit', $role->id) }}"><span class="oi oi-external-link"></span></a></td>
                    <td>
                        <form id="deleteForm{{$role->id}}" method="POST" action="{{ route('role.destroy', $role->id) }}"
                            style="display: none">
                            @csrf
                            @method('DELETE')
                        </form>
                        <a href="#"
                            onclick="event.preventDefault();if(confirm('Are you sure to delete Role#{{$role->id}}?')){document.getElementById('deleteForm{{$role->id}}').submit();}"><span
                                class="oi oi-trash"></span></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                    <td>-</td>
                </tr>
                @endforelse
            </tbody>
            <tfoot>
                <tr>
                    <th>R.Id</th>
                    <th>Name</th>
                    <th>Created by</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection

@section('footer')
<!-- DataTables -->
<script src="{{ asset('admin/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script>
    $(function () {
      $("#roleTable").DataTable({
        "responsive": true,
        "autoWidth": false,
      });
    });
</script>
@endsection