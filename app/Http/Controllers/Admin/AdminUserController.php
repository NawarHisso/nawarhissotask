<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AdminUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $adminUsers = AdminUser::whereHas('roles', function($query){
        //     return $query->where('role_id', '=', 1);
        // })->get();
        if (Gate::forUser(Auth::user())->allows('articles.create')) {
            $adminUsers = AdminUser::doesntHave('roles', 'or', function ($query) {
                return $query->where('role_id', '=', 1);
            })->get();
            return view('admin.users.index', compact('adminUsers'));
        }
        return redirect(route('admin.home'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::forUser(Auth::user())->allows('articles.create')) {
            $roles = Role::all();
            return view('admin.users.user', compact('roles'));
        }
        return redirect(route('admin.home'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users', 'unique:admin_users', 'domain:HaykalMedia.com,gmail.com'],
            'roles' => 'required'
        ]);

        $adminUser = new AdminUser();
        $adminUser->name = $request->name;
        $adminUser->email = $request->email;
        $adminUser->save();

        $adminUser->roles()->sync($request->roles);

        $adminUsers = AdminUser::doesntHave('roles', 'or', function ($query) {
            return $query->where('role_id', '=', 1);
        })->get();
        return view('admin.users.index', compact('adminUsers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::forUser(Auth::user())->allows('articles.create')) {
            $adminUser = AdminUser::with('roles')->where('id', $id)->first();
            $roles = Role::all();
            return view('admin.users.edit', compact('adminUser', 'roles'));
        }
        return redirect(route('admin.home'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'roles' => 'required'
        ]);

        $adminUser = AdminUser::find($id);

        $adminUser->roles()->sync($request->roles);
        $adminUser->save();

        return redirect(route('adminUser.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $adminUser = AdminUser::find($id);
        $user = User::where('email', $adminUser->email)->first();
        AdminUser::where('id', $id)->delete();
        if ($user) {
            $user->delete();
        }

        return redirect(route('adminUser.index'));
    }
}
