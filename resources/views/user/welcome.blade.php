@extends('user.layouts.app')

@section('bg-img', asset('user/img/home-bg.jpg'))

@section('title', 'Clean Blog')

@section('subheading', 'A Blog Theme by Start Bootstrap')

@section('content')

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @foreach ($articles as $article)
            <div class="post-preview">
                <a href="{{ route('article', $article->id) }}">
                    <h2 class="post-title">
                        {{ $article->title }}
                    </h2>
                    <h3 class="post-subtitle">
                        {{ $article->subtitle }}
                    </h3>
                </a>
                <p class="post-meta">Posted by
                    <a href="#">Nawar Hisso</a>
                    {{ $article->created_at->diffForHumans() }}</p>
            </div>
            <hr>
            @endforeach
            <!-- Pager -->
            <div class="clearfix">
                {{ $articles->links() }}
            </div>
        </div>
    </div>
</div>

<hr>

@endsection