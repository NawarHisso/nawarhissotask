<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'subtitle', 'body'
    ];

    public function userCreated()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function userUpdated()
    {
        return $this->belongsTo('App\User', 'updated_by');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'article_tags')->withTimestamps();
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'article_categories')->withTimestamps();
    }

    public function getRouteKeyName()
    {
        return 'id';
    }
}
